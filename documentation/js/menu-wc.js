'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">covid-tracker documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' : 'data-target="#xs-controllers-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' :
                                            'id="xs-controllers-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' : 'data-target="#xs-injectables-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' :
                                        'id="xs-injectables-links-module-AppModule-991b3f5b89f3b0e06a1e1b23de69a1fa9f8bd7043c9bfce843c00cb8eee395c7bc6fbfa09114919834bbda9757c8a3effa5c62ecdbdc352ce03b4185704a647d"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' : 'data-target="#xs-controllers-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' :
                                            'id="xs-controllers-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' : 'data-target="#xs-injectables-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' :
                                        'id="xs-injectables-links-module-UserModule-b590a9ea90fd5a54389bf4caf6bd8d8171202dfc59ee127e0cf328e90042da1b41e32b07f1f4acbdd25ebd963537e932ad3d0b04452318d758ec72348bed3278"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/vaccineModule.html" data-type="entity-link" >vaccineModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' : 'data-target="#xs-controllers-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' :
                                            'id="xs-controllers-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' }>
                                            <li class="link">
                                                <a href="controllers/VaccineController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VaccineController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' : 'data-target="#xs-injectables-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' :
                                        'id="xs-injectables-links-module-vaccineModule-b4abbf387847f332bb7a60bea80659fd0890b739fdfd4f00c008414558a8c0356e7f74b2ccb40df3fcd3b7924a3f0f7f565631f1ef8edbc24d03fa25c8885f14"' }>
                                        <li class="link">
                                            <a href="injectables/VaccineService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VaccineService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/VaccineController.html" data-type="entity-link" >VaccineController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                                <li class="link">
                                    <a href="entities/VaccineData.html" data-type="entity-link" >VaccineData</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/VaccineService.html" data-type="entity-link" >VaccineService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link" >RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});