import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsString } from 'class-validator';
import { Column } from 'typeorm';

export class Maintainance {
  @ApiProperty()
  @IsString()
  @Column()
  createdBy: string;

  @ApiProperty()
  @IsString()
  @Column()
  createdDate: string;

  @ApiProperty()
  @IsString()
  @Column()
  updatedBy: string;

  @ApiProperty()
  @IsString()
  @Column()
  updatedDate: string;
}
