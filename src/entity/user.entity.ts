import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsInt,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';
import { Maintainance } from './maintainance.entity';
import { VaccineData } from './vaccinedata.entity';

/**
 * user entity
 */
@Entity()
export class User extends Maintainance {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * name property
   */
  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  /**
   * aadhar no property
   */
  @ApiProperty()
  @IsString()
  @Column({ unique: true })
  aadharNo: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  @Column()
  password: string;

  /**
   * infected property
   */
  @ApiProperty()
  @IsBoolean()
  @Column()
  infected: boolean;

  /**
   * age property
   */
  @ApiProperty()
  @IsInt()
  @Column()
  age: number;

  /**
   * gender property
   */
  @ApiProperty()
  @IsString()
  @Column()
  gender: string;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;

  @OneToOne(() => VaccineData, (vaccinationdata) => vaccinationdata.user, {
    cascade: true
  })
  vaccinationdata: VaccineData;
}
