import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { VaccineData } from '../entity/vaccinedata.entity';

@Entity()
export class VaccineType {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  vaccineName: string;

  @OneToMany(() => VaccineData, (vaccineData) => vaccineData.vaccineType)
  vaccineDetails: string;
}
