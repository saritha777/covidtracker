import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsInt, isString, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Maintainance } from './maintainance.entity';
import { User } from './user.entity';
import { VaccineType } from '../entity/vaccineType.entity';

/**
 * vaccine data entity
 */
@Entity()
export class VaccineData extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  aadharNo: string;

  @Column()
  vaccineName: string;

  @ApiProperty()
  @IsInt()
  @Column()
  doseNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  // doseDate: Date().toISOString().slice(0, 19).replace('T', ' ');
  doseDate: string;

  @ApiProperty()
  @IsString()
  @Column()
  vaccineStatus: string;

  @OneToOne(() => User, (user) => user.vaccinationdata, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  user: User;

  @ManyToOne(() => VaccineType, (vaccineType) => vaccineType.vaccineDetails, {
    cascade: true
  })
  @JoinColumn()
  vaccineType: VaccineType;

  //@BeforeInsert()
  // async changeDate() {
  //   this.createdDate = moment().format('MMM Do YY');
  //   this.updatedDate = moment().format('MMM Do YY');
  // }
}
