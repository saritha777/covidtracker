import { Body, Controller, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { VaccineType } from 'src/entity/vaccineType.entity';
import { VaccineTypeService } from './vaccinetype.service';

/**
 * vaccine type controller
 */
@ApiTags('vaccine type')
@Controller('/vaccinetype')
export class VaccineTypeController {
  /**
   * injecting service for business logics
   * @param covidService injecting service
   */
  constructor(private readonly vaccineTypeService: VaccineTypeService) {}

  /**
   * post method for vaccine data
   * @param id user id from user
   * @param vaccinedata taking vaccine data
   * @returns vaccine details
   */
  @Post()
  async addVaccineType(@Body() vaccineType: VaccineType) {
    return await this.vaccineTypeService.addVaccineType(vaccineType);
  }
}
