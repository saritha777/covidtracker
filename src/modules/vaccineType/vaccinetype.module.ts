import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VaccineType } from '../../entity/vaccineType.entity';
import { VaccineTypeController } from './vaccinetype.controller';
import { VaccineTypeService } from './vaccinetype.service';

@Module({
  imports: [TypeOrmModule.forFeature([VaccineType])],
  controllers: [VaccineTypeController],
  providers: [VaccineTypeService]
})
export class vaccineTypeModule {}
