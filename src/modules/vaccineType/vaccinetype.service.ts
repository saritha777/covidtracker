import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { VaccineType } from '../../entity/vaccineType.entity';
import { Repository } from 'typeorm';

/**
 * vaccine type service having business logics
 */
@Injectable()
export class VaccineTypeService {
  /**
   * injecting repository
   * @param vaccineTypeRepository having info about vaccine type
   */
  constructor(
    @InjectRepository(VaccineType)
    private vaccineTypeRepository: Repository<VaccineType>
  ) {}

  /**
   * add method for vaccine type
   * @param data taking vaccine type from vaccine type entity
   * @returns
   */
  async addVaccineType(data: VaccineType) {
    return await this.vaccineTypeRepository.save(data);
  }
}
