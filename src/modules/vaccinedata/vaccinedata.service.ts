import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entity/user.entity';
import { VaccineData } from 'src/entity/vaccinedata.entity';
import { VaccineType } from 'src/entity/vaccineType.entity';
import { Repository } from 'typeorm';

/**
 * Providers can be injected into other classes via constructor parameter injection
 */
@Injectable()
export class VaccineService {
  /**
   *injecting repository
   * @param userRepository injecting user repository
   * @param vaccinationRepository injecting vaccine data repository
   * @param vaccineTypeRepository injecting vaccine type repository
   */
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(VaccineData)
    private vaccinationRepository: Repository<VaccineData>,
    @InjectRepository(VaccineType)
    private vaccineTypeRepository: Repository<VaccineType>
  ) {}

  /**
   * add method for vaccine details
   * @param id taking id from user data
   * @param typeId taking id from vaccine type
   * @param data taking vaccine deatails
   * @returns vaccination details
   */
  async addVaccinedata(
    id: number,
    typeId: number,
    data: VaccineData
  ): Promise<VaccineData> {
    const vaccineData = new VaccineData();
    const userData: User = await this.userRepository.findOne({ id: id });
    const vaccineTypeData: VaccineType =
      await this.vaccineTypeRepository.findOne({ id: typeId });
    if (!userData) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    } else {
      const aadhar = (data.aadharNo = userData.aadharNo);
      const vaccName = (data.vaccineName = vaccineTypeData.vaccineName);
      vaccineData.doseDate = data.doseDate;
      vaccineData.doseNo = data.doseNo;
      vaccineData.userId = data.userId;
      vaccineData.vaccineName = vaccName;
      vaccineData.vaccineStatus = data.vaccineStatus;
      vaccineData.createdBy = data.createdBy;
      vaccineData.createdDate = data.createdDate;
      vaccineData.updatedBy = data.updatedBy;
      vaccineData.updatedDate = data.updatedDate;
      vaccineData.aadharNo = aadhar;
      vaccineData.user = userData;
      vaccineData.vaccineType = vaccineTypeData;
      return await this.vaccinationRepository.save(vaccineData);
    }
  }
  /**
   * write getAll() method for all user details
   * @returns getting all details of user
   */
  async getAllVaccinedata(): Promise<VaccineData[]> {
    return this.vaccinationRepository.find();
  }

  /**
   * get method for vaccine status
   * @param vaccineStatus taking status
   * @returns vaccine details based on staus
   */
  async getDataByStatus(vaccineStatus: string): Promise<VaccineData[]> {
    return await this.vaccinationRepository.find({
      vaccineStatus: vaccineStatus
    });
  }

  /**
   * method for get data by date
   * @param date passing dose date
   * @returns all vaccine details
   */
  async getDataByDate(date: string) {
    return await this.vaccinationRepository.find({ doseDate: date });
  }
  /**
   *get method for vaccine name
   * @param vaccineName taking vaccine name
   * @returns vaccine details based on vaccine name
   */
  async getByVaccineName(vaccineName: string): Promise<VaccineData[]> {
    return await this.vaccinationRepository.find({
      vaccineName: vaccineName
    });
  }

  /**
   * update method for vaccine data
   * @param id taking id to update
   * @param data taking vaccine data
   * @returns updated details
   */
  async update(id: number, typeId: number, data: VaccineData) {
    if (!data) {
      throw new HttpException('data not found', HttpStatus.NOT_FOUND);
    } else {
      const vaccineData = new VaccineData();
      const userdata: User = await this.userRepository.findOne({ id: id });
      const vaccineTypeData: VaccineType =
        await this.vaccineTypeRepository.findOne({ id: typeId });
      const aadhar = (data.aadharNo = userdata.aadharNo);
      const vaccName = (data.vaccineName = vaccineTypeData.vaccineName);
      vaccineData.doseDate = data.doseDate;
      vaccineData.doseNo = data.doseNo;
      vaccineData.userId = data.userId;
      vaccineData.vaccineName = vaccName;
      vaccineData.vaccineStatus = data.vaccineStatus;
      vaccineData.createdBy = data.createdBy;
      vaccineData.createdDate = data.createdDate;
      vaccineData.updatedBy = data.updatedBy;
      vaccineData.updatedDate = data.updatedDate;
      vaccineData.aadharNo = aadhar;
      vaccineData.user = userdata;
      await this.vaccinationRepository.update({ id: id }, data);
      return 'Updated vaccination data successfully';
    }
  }

  /**
   * delete method
   * @param id taking id to delete
   * @returns deleted status
   */
  async delete(id: number) {
    await this.vaccinationRepository.delete({ id: id });
    return ' Deleted data successfully';
  }
}
