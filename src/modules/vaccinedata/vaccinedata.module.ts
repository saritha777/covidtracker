import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VaccineType } from '../../entity/vaccineType.entity';
import { User } from '../../entity/user.entity';
import { VaccineData } from '../../entity/vaccinedata.entity';
import { VaccineController } from './vaccinedata.controller';
import { VaccineService } from './vaccinedata.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, VaccineData, VaccineType])],
  controllers: [VaccineController],
  providers: [VaccineService]
})
export class vaccineModule {}
