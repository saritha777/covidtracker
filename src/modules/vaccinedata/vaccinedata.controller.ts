import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { VaccineData } from '../../entity/vaccinedata.entity';
import { VaccineService } from './vaccinedata.service';

/**
 * controller communication betten backend and front end
 */
@ApiTags('vaccine data')
@Controller('/vaccineData')
export class VaccineController {
  /**
   * injecting service for business logics
   * @param covidService injecting service
   */
  constructor(private readonly vaccineService: VaccineService) {}

  /**
   * post method for vaccine details
   * @param id taking user data
   * @param vaccineTypeId taking vccine type id
   * @param vaccinedata taking vaccine data
   * @returns vaccine details
   */
  @Post('/:id/:vaccineTypeId')
  async addVaccinedata(
    @Param('id') id: number,
    @Param('vaccineTypeId') vaccineTypeId: number,
    @Body()
    vaccinedata: VaccineData
  ) {
    return await this.vaccineService.addVaccinedata(
      id,
      vaccineTypeId,
      vaccinedata
    );
  }

  /**
   * get method for all vaccine details
   * @returns all vaccine details
   */
  @Get()
  getAllVaccinedata(): Promise<VaccineData[]> {
    return this.vaccineService.getAllVaccinedata();
  }

  /**
   * method to find data based on status
   * @param vaccineStatus taking vaccine status as input
   * @returns user data based on vaccine status
   */
  @Get('status/:vaccineStatus')
  getDataByStatus(@Param('vaccineStatus') vaccineStatus: string) {
    return this.vaccineService.getDataByStatus(vaccineStatus);
  }

  /**
   * method for get data by date
   * @param date passing dose date
   * @returns all vaccine details
   */
  @Get('date/:date')
  async getDataByDate(@Param('date') date: string) {
    return this.vaccineService.getDataByDate(date);
  }
  /**
   *method to find data based on name
   * @param vaccineName taking vaccine name as input
   * @returns user data based on vaccine name
   */
  @Get('vaccineName/:vaccineName')
  getByVaccineName(@Param('vaccineName') vaccineName: string) {
    return this.vaccineService.getByVaccineName(vaccineName);
  }
  /**
   * update method for the vaccine data
   * @param id taking id to update
   * @param data taking vaccine data
   * @returns updated details
   */
  @Put('/update/:id/:vaccineTypeId')
  updatedata(
    @Param('id') id: number,
    @Param('vaccineTypeId') vaccineTypeId: number,
    @Body() data: VaccineData
  ) {
    return this.vaccineService.update(id, vaccineTypeId, data);
  }

  /**
   * delete method for the vaccine data
   * @param id taking id to delete
   * @returns deleted data
   */
  @Delete('/delete/:id')
  deletedata(@Param('id') id: number) {
    return this.vaccineService.delete(id);
  }
}
