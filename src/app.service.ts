import { Injectable } from '@nestjs/common';

/**
 * app service having basic info
 */
@Injectable()
export class AppService {
  /**
   * get method
   * @returns string hello world
   */
  getHello(): string {
    return 'Hello World!';
  }
}
