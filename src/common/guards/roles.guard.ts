import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { JwtToken } from '../providers/jwtservice';

/**
 * roles guard class
 */
@Injectable()
export class RolesGuard implements CanActivate {
  /**
   * for injecting objects
   * @param reflector taking reflector
   * @param jwtToken injecting 8jwt token
   */
  constructor(
    private readonly reflector: Reflector,
    private jwtToken: JwtToken,
  ) {}

  /**
   *can activate method for activate
   * @param context interface describing details about the current request pipeline.
   * @returnsboolean or observableas boolean or promise as boolean
   */
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    /**
     * roles object hving the handler
     */
    const roles = this.reflector.get<string>('jwt', context.getHandler());
    console.log(roles);

    //  if (!roles) {
    //    return true;
    //  }

    if (!this.jwtToken.verifyToken(roles)) {
      return false;
    }

    return true;
  }
}
