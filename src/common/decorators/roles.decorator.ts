import { SetMetadata } from '@nestjs/common';

/**
 * custom decorator for set meta data
 * @param role custom decorator
 * @returns metadata
 */
export const Roles = (role) => SetMetadata('roles', role);
