import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  /**
   * title property
   */
  title: 'COVID-TRACKER',
  /**
   * description property
   */
  description: 'covid tracker',
  /**
   * version property
   */
  version: '1.0',
  /**
   * tags property
   */
  tags: ['templete'],
};
